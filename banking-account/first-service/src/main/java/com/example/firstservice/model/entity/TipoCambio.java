package com.example.firstservice.model.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class TipoCambio {

    @JsonProperty("source")
    private String source;
    @JsonProperty("quotes")
    private Map<String, Double>  quotes;
}
