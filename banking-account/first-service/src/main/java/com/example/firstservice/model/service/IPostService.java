package com.example.firstservice.model.service;

import com.example.firstservice.model.entity.Post;

import java.util.List;

public interface IPostService {

    List<Post> saveAll(List<Post> posts);
}
