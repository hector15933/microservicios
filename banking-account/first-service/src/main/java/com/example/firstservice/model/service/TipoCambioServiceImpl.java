package com.example.firstservice.model.service;

import com.example.firstservice.client.feing.IFeingTipoCAmbio;
import com.example.firstservice.model.entity.TipoCambio;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TipoCambioServiceImpl implements ITipoCambioService {

    private final IFeingTipoCAmbio iFeingTipoCAmbio;

    public TipoCambio getTipoCambio(String access_key,  String currencies, String source, Integer format){
        return iFeingTipoCAmbio.getTipoCambio(access_key,currencies,source,format);
    }
}
