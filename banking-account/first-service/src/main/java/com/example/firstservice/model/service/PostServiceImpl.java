package com.example.firstservice.model.service;

import com.example.firstservice.model.dao.PostDao;
import com.example.firstservice.model.entity.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PostServiceImpl implements IPostService{

    @Autowired
    private PostDao postDao;

    @Override
    public List<Post> saveAll(List<Post> posts) {
        return (List<Post>) postDao.saveAll(posts);
    }
}
