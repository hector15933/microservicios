package com.example.firstservice.controller;

import com.example.firstservice.client.feing.IFeingSocialPost;
import com.example.firstservice.model.entity.Post;
import com.example.firstservice.model.entity.TipoCambio;
import com.example.firstservice.model.service.IPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PostController {

    @Autowired
    private IFeingSocialPost iFeingSocialPost;

    @Autowired
    private IPostService iPostService;


    @GetMapping(value = "/posts")
    public List<Post> obtenerPosts(){
        try{

            iPostService.saveAll(iFeingSocialPost.getPost());
            return iFeingSocialPost.getPost();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
}
