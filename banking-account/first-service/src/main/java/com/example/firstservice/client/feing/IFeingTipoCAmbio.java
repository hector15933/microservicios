package com.example.firstservice.client.feing;


import com.example.firstservice.model.entity.TipoCambio;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name="feingTipoCambio", url="${tipo-cambio-service}")
public interface IFeingTipoCAmbio {

    @GetMapping(path="/post")
    TipoCambio getTipoCambio(@RequestParam(value = "access_key") String access_key,@RequestParam(value = "currencies") String currencies,@RequestParam(value = "source") String source,@RequestParam(value = "format") Integer format);
}
