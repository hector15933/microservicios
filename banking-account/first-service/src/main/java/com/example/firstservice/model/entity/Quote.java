package com.example.firstservice.model.entity;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Quote {

    private String currency;
}
