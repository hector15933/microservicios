package com.example.firstservice.client.feing;

import com.example.firstservice.model.entity.Post;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@FeignClient(name="feingSocialPost", url="${social-service}")
public interface IFeingSocialPost {

    @GetMapping(value="/posts")
     List<Post> getPost();

}
