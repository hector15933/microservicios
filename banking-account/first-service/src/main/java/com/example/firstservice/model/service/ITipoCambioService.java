package com.example.firstservice.model.service;

import com.example.firstservice.model.entity.TipoCambio;

public interface ITipoCambioService {

    TipoCambio getTipoCambio(String access_key, String currencies, String source, Integer format);

}
