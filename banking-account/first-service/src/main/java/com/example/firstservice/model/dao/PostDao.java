package com.example.firstservice.model.dao;

import com.example.firstservice.model.entity.Post;
import org.springframework.data.repository.CrudRepository;

public interface PostDao extends CrudRepository<Post,Long>{


}
