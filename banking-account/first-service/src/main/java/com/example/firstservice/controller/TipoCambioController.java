package com.example.firstservice.controller;


import com.example.firstservice.client.feing.IFeingTipoCAmbio;
import com.example.firstservice.model.entity.TipoCambio;
import com.example.firstservice.model.service.ITipoCambioService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class TipoCambioController {


    @Autowired
    private ITipoCambioService iTipoCambioService;



    @GetMapping(value = "/live")
    public TipoCambio obtenerTipoCambio(@RequestParam(value = "access_key") String access_key, @RequestParam(value = "currencies") String currencies, @RequestParam(value = "source") String source, @RequestParam(value = "format") Integer format){

        return iTipoCambioService.getTipoCambio(access_key,currencies,source,format);
    }
}
