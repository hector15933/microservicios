package com.banking.account.cmd.api.command;

import com.banking.account.cmd.api.dto.NotificationType;
import com.banking.cqrs.core.commands.BaseCommand;
import lombok.*;

@Data
@Builder
public class NotificationCommand extends BaseCommand {
    private String content;
    private NotificationType type;
}
