package com.banking.account.cmd.domain;

import com.banking.account.cmd.api.command.NotificationCommand;
import com.banking.account.cmd.api.command.OpenAccountCommand;
import com.banking.account.cmd.api.command.TransferFundsCommand;
import com.banking.account.common.dto.NotificationType;
import com.banking.account.common.events.*;
import com.banking.cqrs.core.domain.AggregateRoot;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
public class AccountAggregate extends AggregateRoot {
    private Boolean active;
    private double balance;

    public double getBalance(){
        return this.balance;
    }

    public AccountAggregate(OpenAccountCommand command){
        raiseEvent(AccountOpenedEvent.builder()
                .id(command.getId())
                .accountHolder(command.getAccountHolder())
                .createdDate(new Date())
                .accountType(command.getAccountType())
                .openingBalance(command.getOpeningBalance())
                .build());
    }

    public void apply(AccountOpenedEvent event){
        this.id = event.getId();
        this.active = true;
        this.balance = event.getOpeningBalance();
    }

    public void depositFunds(double amount){
        if(!this.active){
            throw new IllegalStateException("Los fondos no pueden ser depositados en esta cuenta");
        }

        if(amount <= 0){
            throw new IllegalStateException("El deposito de dinero no puede ser cero menos que cero");
        }

        raiseEvent(FundsDepositedEvent.builder()
                .id(this.id)
                .amount(amount)
                .build());

    }

    public void apply(FundsDepositedEvent event){
        this.id = event.getId();
        this.balance += event.getAmount();
    }


    public AccountAggregate(TransferFundsCommand command){
        raiseEvent(TransferFundsEvent.builder()
                .id(command.getId())
                .amount(command.getAmount())
                .debitAccount(command.getDebitAccount())
                .creditAccount(command.getCreditAccount())
                .sourceCurrency(command.getSourceCurrency())
                .targetCurrency(command.getTargetCurrency()).build());
    }

    public void apply(TransferFundsEvent event){

    }
    public void withdrawFunds(double amount){
        if(!this.active){
            throw new IllegalStateException("La cuenta bancaria esta cerrada");
        }
        raiseEvent(FundsWithdrawnEvent.builder()
                .id(this.id)
                .amount(amount)
                .build());
    }

    public void apply(FundsWithdrawnEvent event){
        this.id = event.getId();
        this.balance -= event.getAmount();
    }

    public AccountAggregate(NotificationCommand command){
        raiseEvent(NotificationEvent.builder()
                .id("bde3d6f3-1b92-4119-aa13-86aed9e9d07e")
                .type(NotificationType.WARN)
                .content(command.getContent())
                .build());
    }

    public void apply(NotificationEvent message){
    }

    public void closeAccount(){
        if(!active){
            throw new IllegalStateException("La cuenta de banco esta cerrada");
        }

        raiseEvent(AccountClosedEvent.builder()
                .id(this.id)
                .build());
    }

    public void apply(AccountClosedEvent event){
        this.id = event.getId();
        this.active = false;
    }


}
