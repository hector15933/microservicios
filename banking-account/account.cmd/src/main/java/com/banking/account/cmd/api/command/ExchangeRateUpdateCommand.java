package com.banking.account.cmd.api.command;

import com.banking.cqrs.core.commands.BaseCommand;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class ExchangeRateUpdateCommand extends BaseCommand {
    private String currencyFrom;
    private String currencyTo;
    private BigDecimal rate;
}
