package com.banking.account.cmd.api.dto;

public enum NotificationType {
    INFO, WARN
}
