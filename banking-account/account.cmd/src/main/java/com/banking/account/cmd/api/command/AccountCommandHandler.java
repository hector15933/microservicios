package com.banking.account.cmd.api.command;

import com.banking.account.cmd.domain.AccountAggregate;
import com.banking.account.common.events.NotificationEvent;
import com.banking.cqrs.core.handlers.EventSourcingHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountCommandHandler implements CommandHandler {
    @Autowired
    private EventSourcingHandler<AccountAggregate> eventSourcingHandler;

    @Override
    public void handle(OpenAccountCommand command) {
        var aggregate = new AccountAggregate(command);
        eventSourcingHandler.save(aggregate);
    }

    @Override
    public void handle(DepositFundsCommand command) {
        var aggregate = eventSourcingHandler.getById(command.getId());
        aggregate.depositFunds(command.getAmount());
        eventSourcingHandler.save(aggregate);
    }

    @Override
    public void handle(WithdrawFundsCommand command) {
        var aggregate = eventSourcingHandler.getById(command.getId());
        if (command.getAmount() > aggregate.getBalance()){
            NotificationCommand notificationCommand =  NotificationCommand.builder().content("Insuficientes fondos, no se puede retirar dinero").build();
            handle(notificationCommand);
          throw new IllegalStateException("Insuficientes fondos, no se puede retirar dinero");
        }

        aggregate.withdrawFunds(command.getAmount());
        eventSourcingHandler.save(aggregate);
    }

    @Override
    public void handle(CloseAccountCommand command) {
        var aggregate = eventSourcingHandler.getById(command.getId());
        aggregate.closeAccount();
        eventSourcingHandler.save(aggregate);
    }

    @Override
    public void handle(TransferFundsCommand command) {
        var aggregateDebit = eventSourcingHandler.getById(command.getDebitAccount());
        if (command.getAmount() > aggregateDebit.getBalance()){

            NotificationCommand notificationCommand =  NotificationCommand.builder().content("Insuficientes fondos, no se puede transferir dinero").build();
            handle(notificationCommand);
            throw new IllegalStateException("Insuficientes fondos, no se puede transferir dinero");

        }
        var aggregateCredit = eventSourcingHandler.getById(command.getCreditAccount());
        var aggregateTransfer = new AccountAggregate(command);

        aggregateDebit.withdrawFunds(command.getAmount());
        aggregateCredit.depositFunds(command.getAmount());
        //aggregateDebit.transferFunds(command);

        eventSourcingHandler.save(aggregateDebit);
        eventSourcingHandler.save(aggregateCredit);
        eventSourcingHandler.save(aggregateTransfer);
    }

    @Override
    public void handle(NotificationCommand command) {
        var aggregate = new AccountAggregate(command);
        eventSourcingHandler.save(aggregate);
    }
}
