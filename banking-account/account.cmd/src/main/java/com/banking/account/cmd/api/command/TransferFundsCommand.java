package com.banking.account.cmd.api.command;

import com.banking.cqrs.core.commands.BaseCommand;
import lombok.Data;

@Data
public class TransferFundsCommand extends BaseCommand {
    private double amount;
    private String debitAccount;
    private String creditAccount;
    private String sourceCurrency;
    private String targetCurrency;

}
