package com.notification.service.kafkaconsumer.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class Notification {
   @JsonProperty("id")
   private String id;
   @JsonProperty("content")
   private String content;
   @JsonProperty("type")
   private NotificationType type;
   @JsonProperty("version")
   private String version;
}
