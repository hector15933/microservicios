package com.banking.account.common.events;

import com.banking.account.common.dto.NotificationType;
import com.banking.cqrs.core.events.BaseEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class NotificationEvent extends BaseEvent {
    private String content;
    private NotificationType type;
}