package com.banking.account.common.events;


import com.banking.cqrs.core.events.BaseEvent;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class TransferFundsEvent extends BaseEvent {
    private double amount;
    private String debitAccount;
    private String creditAccount;
    private String sourceCurrency;
    private String targetCurrency;
}
