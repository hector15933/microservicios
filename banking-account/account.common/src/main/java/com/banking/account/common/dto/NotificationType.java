package com.banking.account.common.dto;

public enum NotificationType {
    INFO, WARN
}
