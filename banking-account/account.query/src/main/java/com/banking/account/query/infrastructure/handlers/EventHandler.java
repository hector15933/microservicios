package com.banking.account.query.infrastructure.handlers;

import com.banking.account.common.events.*;

public interface EventHandler {
    void on(AccountOpenedEvent event);
    void on(FundsDepositedEvent event);
    void on(FundsWithdrawnEvent event);
    void on(AccountClosedEvent event);
    void on(TransferFundsEvent event);
    void on(NotificationEvent event);
}