package com.banking.account.query.domain;

import com.banking.account.common.events.TransferFundsEvent;
import org.springframework.data.repository.CrudRepository;

public interface TransferRepository extends CrudRepository<TransferFunds,Long> {
}
